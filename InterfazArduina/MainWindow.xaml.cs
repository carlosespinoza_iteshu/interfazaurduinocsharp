﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace InterfazArduina
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer _timer;
        private string _mensaje;
        private string _error;
        private PtoSerie _puerto;
        public MainWindow()
        {
            InitializeComponent();
            ActualizarPuertos();
            _timer = new DispatcherTimer();
            _timer.Interval = new TimeSpan(0, 0,0, 1,10);
            _timer.Tick += Timer_Tick;
            _timer.Start();
        }

        private void ActualizarPuertos()
        {
            cmbPuertos.ItemsSource = System.IO.Ports.SerialPort.GetPortNames();
            if(cmbPuertos.Items.Count==0)
            {
                EscribirLog("No se encontraron puertos habilitados");
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(_error))
            {
                EscribirLog("Error: " + _error);
                _error = string.Empty;
            }
            if (!string.IsNullOrEmpty(_mensaje))
            {
                EscribirLog(_mensaje);
                _mensaje = string.Empty;
            }
        }

        

        private void chkConectar_Checked(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty( cmbPuertos.Text))
            {
                EscribirLog("Error: No hay puerto seleccionado");
                return;
            }
            _puerto = new PtoSerie(cmbPuertos.Text);
            _puerto.Error += Puerto_Error;
            _puerto.Mensaje += Puerto_Mensaje;
            if(_puerto.Conectar())
            {
                EscribirLog("Puerto Conectado");
            }
            else
            {
                EscribirLog("Error no se pudo conectar");
            }
        }

        private void Puerto_Mensaje(object sender, EventArgs e)
        {
            _mensaje = sender.ToString();
        }

        private void Puerto_Error(object sender, EventArgs e)
        {
            _error = sender.ToString();
        }

        private  void EscribirLog(string v)
        {
            lstConsola.Items.Add(string.Format("{0}: {1}", DateTime.Now.ToShortTimeString(), v));
        }

        private void chkConectar_Unchecked(object sender, RoutedEventArgs e)
        {
            if (_puerto != null)
            {
                if (_puerto.Desconectar())
                {
                    EscribirLog("Puerto desconectado");
                }
                else
                {
                    EscribirLog("Error no se pudo desconectar");
                }
            }
            else
            {
                EscribirLog("Error: No existe puerto abierto para ser cerrado.");
            }
        }

        private void btnActualizar_Click(object sender, RoutedEventArgs e)
        {
            ActualizarPuertos();
        }

        private void btnEnviar_Click(object sender, RoutedEventArgs e)
        {
            if(_puerto!=null)
            {
                _puerto.Escribir(txtMensaje.Text);
            }
            else
            {
                EscribirLog("Error: No hay puerto abierto para escribir");
            }
        }
    }
}
