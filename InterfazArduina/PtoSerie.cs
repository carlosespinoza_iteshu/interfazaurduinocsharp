﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;


namespace InterfazArduina
{
    public class PtoSerie
    {
        private SerialPort _pto;

        public event EventHandler Error;
        public event EventHandler Mensaje;

        public string Puerto { get; private set; }

        public PtoSerie(string puerto)
        {
            this.Puerto = puerto;
            _pto = new SerialPort(this.Puerto);
            _pto.Parity = Parity.None;
            _pto.BaudRate = 9600;
            _pto.StopBits = StopBits.One;
            _pto.ReadTimeout = 1000;
            _pto.WriteTimeout = 1000;
            _pto.Handshake = Handshake.None;
            _pto.ReadBufferSize = 64;
            _pto.WriteBufferSize = 64;
            _pto.DtrEnable = true;
            _pto.RtsEnable = true;
            _pto.DataReceived += _pto_DataReceived;
        }
        
        public void Escribir(string cadena)
        {
            _pto.WriteLine(cadena);
        }

        public bool Conectar()
        {
            try
            {
                _pto.Open();
                return true;
            }
            catch (Exception ex)
            {
                this.Error(ex.Message , null);
                return false;
            }
        }

        public bool Desconectar()
        {
            try
            {
                _pto.Close();
                return true;
            }
            catch (Exception ex)
            {
                this.Error(ex.Message, null);
                return false;
            }
        }

        private void _pto_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            this.Mensaje(_pto.ReadExisting(), null);
        }
    }
}
